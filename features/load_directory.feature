Feature: Load a directory
    In order to simplify discovery of avro schema files
    As a PHP developer
    I need to be able to specify a directory in which to look for *.avsc files

    Scenario: Successfully generate multiple class from a directory containing schemas
        Given the following "schemas/message_a.avsc" file:
        """
        {
            "namespace": "com.jaumo",
            "name": "MessageA",
            "type": "record",
            "doc": "The Avro message",
            "fields": [
                {
                    "name": "my_prop",
                    "type": { "type": "record", "name": "my_prop", "fields": [{ "name": "foo", "type": "string" }] }
                }
            ]
        }
        """
        And the following "schemas/message_b.avsc" file:
        """
        {
            "namespace": "com.jaumo",
            "name": "MessageB",
            "type": "record",
            "doc": "The Avro message",
            "fields": [
                {
                    "name": "my_prop",
                    "type": { "type": "record", "name": "my_prop", "fields": [{ "name": "foo", "type": "string" }] }
                }
            ]
        }
        """
        And the following "schemas/message_c.avsc" file:
        """
        {
            "namespace": "com.jaumo",
            "name": "MessageC",
            "type": "record",
            "doc": "The Avro message",
            "fields": []
        }
        """
        When I execute "generate schemas/"
        Then file "/build/Com/Jaumo/MessageA.php" should exist
        Then file "/build/Com/Jaumo/MessageB.php" should exist
        Then file "/build/Com/Jaumo/MessageC.php" should exist
