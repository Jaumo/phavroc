Feature: Output directory overriding
    In order to own my project's directory structure
    As a PHP developer
    I need to be able to define the output directory in which to write PHP classes

    Scenario: Successfully override output directory
        Given the following "message.avsc" file:
        """
        {
            "namespace": "com.jaumo",
            "name": "Message",
            "type": "record",
            "doc": "The Avro message",
            "fields": []
        }
        """
        When I execute "generate message.avsc src"
        Then file "/src/Com/Jaumo/Message.php" should exist
