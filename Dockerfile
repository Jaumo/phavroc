FROM php:8.1-cli

# Setup env
ADD opt/setup-dev-container.sh /root/setup-dev-container.sh
RUN /root/setup-dev-container.sh

